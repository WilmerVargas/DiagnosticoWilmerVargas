/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DiagnosticoWilmerVargas;

/**
 *
 * @author Wilmer
 */
public class Matriz {

    private int[][] matriz;
    int filas;
    int columnas;
    
//    public Matriz(int[][] Matriz){
//        this.matriz = Matriz;
//        
//    }
//   public void setMatrizAllanWay(int filas, int columnas) {
//        this.matriz = new int[filas][columnas];
//        for (int i = 0; i < matriz.length; i++) {
//            for (int j = 0; j < matriz.length; j++) {
//                this.matriz[i][j] = 1;
//            }
//
//        }
//    }
    /**
     * llena la matriz con valores de 1
     *
     * @param filas
     * @param columnas
     */
    public void setMatriz(int filas, int columnas) {
        this.matriz = new int[filas][columnas];
        this.filas = filas;
        this.columnas = columnas;
        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                this.matriz[i][j] = 1;
            }

        }
    }
    /***
    /**
     * asigna a la matriz numeros random en las posiciones de esta desechando
     * los unos
     *
     * @param numero el rango que el usuario quiere que este los numeros random
     * que se generan;
     */
    public void asignarDatos(int numero) {
        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                this.matriz[i][j] = (int) (Math.random() * numero);

            }
        }
    }

    /**
     * determina el numero mayor de la matriz
     *
     * @return el numero mayor que se encuentra en la matriz
     */
    public int numMayor() {
        int mayor = matriz[0][0];
        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                if (matriz[i][j] > mayor) {
                    mayor = matriz[i][j];
                }
            }

        }
        return mayor;
    }

    public int numMenor() {
        int menor = matriz[0][0];
        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                if (matriz[i][j] < menor) {
                    menor = matriz[i][j];
                }
            }

        }
        return menor;
    }
    @Override
    public String toString() {
        String resultado = "";
        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                resultado += matriz[i][j] + "\t";
            }
            resultado += "\n";
        }
        return resultado;
    }
    
}
