/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DiagnosticoWilmerVargas;

/**
 *
 * @author estudiante
 */
//ejercicio 3
public class Logica {
    String variable1;
    char variable2;
    int variable3;
    double variable4;
    byte variable5;
    float variable6;

    public Logica() {
        variable1 = "Wilmer";
        variable2 = 'W';
        variable3 = 23;
        variable4 = 23.3;
        variable5 = 100;
        variable6 = 1.18f;
        
    }
                    
    Estudiante estudiante;

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    public Logica(String Nombre, String ApellidoUno, String ApellidoDos, String Cedula, String Celular, String Direccion) {
        this.estudiante = new Estudiante(Nombre, ApellidoUno, ApellidoDos, Cedula, Celular, Direccion);
    }
    //ssad
    //ejercicio 1 
    /**
     * Sirver para eliminar y contar los espacio que ingreso el usuario en la frase digitada
     * @param texto es la frase que digito el usuario
     * @return deuvelve la frase sin espacios y la cantidad de espacios que habia
     */
    
    public String EliminarEspaciosYcontarlos (String texto){
        
        
        int contador = 0;
        for (int i = 0; i <texto.length(); i++) {     
            if (texto.charAt(i)==' ') {
              contador++;  
            }    
        }
        String resultado = "Frase sin espacios: "+ texto.trim().replace(" ", "")+"\n"
                +"Numero de espacios "+ contador + " ";
        return resultado;
    }
    
    //ejercicio 2
    /**
     * sirve par invertir la frase que digito el usuario
     * @param texto frase digitada por el usuario
     * @return devuelver la frase invertida
     */
    public String InvertirFrase (String texto){
        String cadena = " ";
        for (int i = 0; i < texto.length(); i++) {
            cadena = texto.charAt(i) + cadena;
        }
      return "frase invertida "+cadena;
    }
   
    //ejercicio 4
    /**
     * Le asigna al arregloCaracteres una letra en x posicion
     * @return devuelve el arreglo con letras y la posicion de estas
     */
    public String arregloCaracteres (){
    char[]arregloCaracteres = {'a','b','c','d','e','f','g'};
    String datos= "";
    
    for (int i = 0; i < arregloCaracteres.length; i++) {
            datos += "posicion: "+ i + " = " +  arregloCaracteres[i] +"\n";
        }
    return datos;
    }
    public String declaracionVariables(){
        String variable1 = "Wilmer";
    char variable2 = 'W';
    int variable3 = 23;
    double variable4 = 65.5;
    byte variable5= 100;
    float variable6 = 69.9f;
    
    return  "Metodo con return"+ "\n"+
            variable1 + "\n"+
            variable2 + "\n"+
            variable3 + "\n"+
            variable4 + "\n"+
            variable5 + "\n"+
            variable6 + "\n";       
    }
}
