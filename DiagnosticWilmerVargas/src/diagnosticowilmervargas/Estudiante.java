/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DiagnosticoWilmerVargas;

/**
 *
 * @author estudiante
 */
public class Estudiante {
    /**
     * Seleccione dichos atributos ya que pienso que el manejo de informacion personal es de suma importancia para cualquier 
     * programa
     */
    private String Nombre;
    private String ApellidoUno;
    private String ApellidoDos;
    private String Cedula;
    private String Celular;
    private String Direccion;

    public Estudiante(String Nombre, String ApellidoUno, String ApellidoDos, String Cedula, String Celular, String Direccion) {
        this.Nombre = Nombre;
        this.ApellidoUno = ApellidoUno;
        this.ApellidoDos = ApellidoDos;
        this.Cedula = Cedula;
        this.Celular = Celular;
        this.Direccion = Direccion;
    }
    @Override
    public  String toString(){
      return Nombre +" "+ ApellidoUno +" "+ ApellidoDos +" "+ Cedula +" "+ Celular +" "+ Direccion;  
    } 

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellidoUno() {
        return ApellidoUno;
    }

    public void setApellidoUno(String ApellidoUno) {
        this.ApellidoUno = ApellidoUno;
    }

    public String getApellidoDos() {
        return ApellidoDos;
    }

    public void setApellidoDos(String ApellidoDos) {
        this.ApellidoDos = ApellidoDos;
    }

    public String getCedula() {
        return Cedula;
    }

    public void setCedula(String Cedula) {
        this.Cedula = Cedula;
    }

    public String getCelular() {
        return Celular;
    }

    public void setCelular(String Celular) {
        this.Celular = Celular;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

  
    
    
}
